Description
===========
This module redirect "sku/[sku]" alias to correct "node/[node]" page.
In the configuration page you can change "sku" to whatever you want.

Requirements
============
uc_directaccess requires Ubercart.

Installation
============
Copy the 'uc_directaccess' module directory in to your Drupal
sites/all/modules/ubercart directory as usual.

User permissions
================
You have to give access rights to user roles for administer 
this module.
DONT FORGET TO SET PERMISSIONS!

Usage
=====
You can use link like
www.example.com/?=sku/123ABC
or (with Clean URLs)
www.example.com/books/I9780262530910

and you will be redirected to correct product page.
